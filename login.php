<?php
require'connect.php';
require 'functions.php';
session_start();
if (!empty($_POST['doSignUp'])){
    doSignUp($pdo,$_POST['login'],($_POST['pass']));
}

if (!empty($_POST['checkUser'])){
    $userExist = checkUser($pdo,$_POST['login'],($_POST['pass']));
    if($userExist){
        $_SESSION['user'] = $userExist;
        echo'<meta http-equiv="refresh" content="0; url=index.php">';
        die;
    }
    echo $userExist;
}
?>

<!DOCTYPE html>
<head>
	<title>Авторизация</title>
	<meta charset="UTF-8" />
	<link href="style.css" rel="stylesheet" />
</head>

<body>
<form id="loginForm" action="login.php" method="post">

	<div class="field">
		<label>Имя пользователя:</label>
                <div class="input"><input type="text" name="login" value="" id="login" required="required"></div>
	</div>

	<div class="field">
		<!--<a href="#" id="forgot">Забыли пароль?</a>-->
		<label>Пароль:</label>
		<div class="input"><input type="password" name="pass" value="" id="pass" required="required"></div>
	</div>

	<div class="submit">
            <button type="submit" name="checkUser" value="DO!">Войти</button>&nbsp;&nbsp;&nbsp;
            <button type="submit" name="doSignUp" value="DO!" style="float: left">Зарегестрировать</button>
	</div>

</form>

</body>
</html>