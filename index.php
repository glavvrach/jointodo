<?php
        require'connect.php';
        require 'functions.php';
        needAuth();
        //print_r($_SESSION);
        $message = '<span class="default alert">Добавьте новую задачу</span>';
        $utf8 = $pdo->query("SET NAMES 'utf8';");
        $use = $pdo->query("USE $useDB");
        
                
                if(isset($_GET['task'])&&!empty($_GET['task'])){
                    $task = $_GET['task'];
                }else{
                    if(isset($_GET['task'])&&empty($_GET['task'])){
                    $message = '<span class="default alert">Задана пустая задача</span>';
                    }
                }

                $sort = isset($_GET['selectSort'])  ? $_GET['selectSort']  : 'id';
                $allowed = array("id", "date_added", "description","is_done"); //белый список(подсмотрено на хабре)
                $key = array_search($sort,$allowed);
                $orderBy=$allowed[$key];
                
                if(isset($_GET['action'])&&(($_GET['action'])=='delete')){
                    delete($pdo,intval($_GET['id']));
                    $message = '<span class="danger alert">Задачa удалена!</span>';
                }
                
                if(isset($_GET['action'])&&(($_GET['action'])=='doIt')){
                    doIt($pdo,intval($_GET['id']));
                    $message = '<span class="success alert">Задачa помечена как выполненная!</span>';
                }
                
                if (isset($_POST['changeTask'])&&isset($_POST['newValue'])){
                    changeTask($pdo,intval($_POST['id']),$_POST['newValue']);
                    $message = '<span class="success alert">Задачa изменена!</span>';
                }
                
                if (isset($_POST['transmit']) && !empty($_POST['selectNewUser']) ){
                    //print_r($_POST['selectNewUser']);print_r($_POST['taskId']);exit;
                    transmit($pdo,intval($_POST['selectNewUser']),intval($_POST['taskId']));
                    $message = '<span class="success alert">Задачa передана!</span>';
                }
                //print_r($_POST);
        ?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
        <title>Document</title>
    </head>
    <body> 
        <div class="wrapper">
            <div class="mainAdmin">
        <h1>Список дел TODO созданный вами</h1>
        <div style="float: right">
        <?php
        echo 'Привет '.$_SESSION['user']['login']
                .' '.'<a href="logout.php">Выйти</a>';
        ?></div>
        <div style="float: left;">
        <?php
            if(isset($_GET['action'])&&($_GET['action'])=='change'){
            $val = read($pdo,intval($_GET['id']));
            $id = intval($_GET['id']);
            ?><form action="" method="POST">
            <input type="text" name="newValue" placeholder="Выберите задание" value="<?php echo $val;?>">
            <input type="submit" name="changeTask" value="Изменить задачу">
            <input type="hidden" name="id" value="<?php echo $id;?>">
         </form><?php
        }else{?>
            <form action="">
            <input type="text" name="task" placeholder="Название задания">
            <input type="submit" name="create" value="Добавить">
            </form>
        <?php }?>
        </div>    
        <div style="float: left; padding-left: 20px;">
            <form action="">
                <select name="selectSort"><option value="id">Сортировка по умолчанию</option>
                <option value="date_added">Сортировать по дате добавления</option>
                <option value="description">Сортировать по описанию</option>
                <option value="is_done">Сортировать по статусу</option></select>
                <input type="submit" name="sort" value="Сортировать">
            </form>
        </div>    
        <br>
        <div><?php echo '<div id = "Message">'.$message.'</div>';?></div>
        <table width ="100%">
            <thead style="background:lightgray;">
                <tr><td>Задача</td><td>Статус</td><td>Добавлена</td><td>Действия</td><td>Передать</td></tr>
            </thead>
            
        <?php
        global $useDB,$taskTable,$SUID;
        $showAll = $pdo->query("SELECT * FROM $useDB.$taskTable WHERE user_id= $SUID ORDER BY $orderBy");
                foreach( $showAll  as  $tasks ){
                    
                    if($tasks['is_done']==0){
                        $isDone = 'Надо выполнять';
                        $bg = 'danger';
                            }else{if($tasks['is_done']==1){
                                    $isDone = 'Дело выполнено!';
                                    $bg = "success";
                                }
                            }
                
                    if($task==$tasks['description']){
                        $exist = 1;
                    }
         ?>
            <tr><td><?php echo $tasks['description']?></td>
                    <td class=<?php echo $bg.' style ="display: table-cell;">'.$isDone; ?></td>
                    <td><?php echo $tasks['date_added']; ?></td>
                    <td><a href="/?action=doIt&id=<?php echo $tasks['id'];?>&selectSort=<?php echo $sort;?>"> Выполнить</a>&nbsp;
                    <a href="/?action=delete&id=<?php echo $tasks['id'];?>&selectSort=<?php echo $sort;?>"> Удалить</a><br>
                    <a href="/?action=change&id=<?php echo $tasks['id'];?>&selectSort=<?php echo $sort;?>"> Изменить</a>&nbsp;</td><td>
                        
            <form action="/" method="POST">
                <select name="selectNewUser"><option value="">Выберите пользователя</option>
                    <?php
                    $u = $pdo->query("SELECT id_user,login FROM $useDB.$userTable");
                    while ($user = $u->fetch(PDO::FETCH_ASSOC)){
                        echo'<option value="'.$user['id_user'].'">'.$user['login'].'</option>';
                    }
                    ?>
                    <input type="hidden" name="taskId" value="<?php echo $tasks['id'];?>">
                    <input type="submit" name="transmit" value="Сменить" style="margin-left: 5px;">
            </form></td></tr>
                        <?php 
        }
        if(($exist == 0) &&(!empty($task))){
            create($pdo,$task);
            }else{  
              if($exist == 1){
               duplicate();
            }
        }
        ?>
                 
            
        </table>
        <!-- ------------------------Переданные дела---------------------------------------- -->
        <h1>Список дел которые вам передали</h1>
        <table width ="100%">
            <thead style="background:lightgray;">
                <tr><td>Задача</td><td>Статус</td><td>Добавлена</td><td>Действия</td></tr>
            </thead>
             <tbody>
        <?php
        global $useDB,$taskTable,$SUID;
        $showAll = $pdo->query("SELECT * FROM $useDB.$taskTable JOIN $useDB.$userTable "
                . "ON assigned_user_id = $userTable.id_user AND assigned_user_id = $SUID ORDER BY $taskTable.$orderBy ");
        
//        $showAll->fetch_assoc();
             while ($tasks = $showAll->fetch(PDO::FETCH_ASSOC)){
                    //print_r($tasks);
                    if($tasks['is_done']==0){
                        $isDone = 'Надо выполнять';
                        $bg = 'danger';
                }else{if($tasks['is_done']==1){
                        $isDone = 'Дело выполнено!';
                        $bg = "success";
                    
                     }
                }
          $u = $tasks['user_id'];   
          $creator = $pdo->query("SELECT login FROM $useDB.$userTable WHERE id_user = $u");
          while($row = $creator->fetch(PDO::FETCH_ASSOC)){
              $cre = $row['login'];
          }?>
        <tr><td><?php echo $tasks['description'];?></td>
                    <td class=<?php echo $bg.' style ="display: table-cell;">'.$isDone;?></td>
                    <td><?php echo $tasks['date_added'].' Добавил '.$cre;?></td>
                    <td><a href="/?action=doIt&id=<?php echo $tasks['id'];?>&selectSort=<?php echo $sort;?>"> Выполнить</a>&nbsp;
                    <a href="/?action=delete&id=<?php echo $tasks['id'];?>&selectSort=<?php echo $sort;?>"> Удалить</a>&nbsp;
                    <a href="/?action=change&id=<?php echo $tasks['id'];?>&selectSort=<?php echo $sort;?>"> Изменить</a>&nbsp;</td>
                    </tr>
        <?php }
        
               
        ?>
                 
            </tbody>
        </table>
        </div>
            
       </div>
        
    </body>
</html>