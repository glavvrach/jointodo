<?php
session_start();
$SUID = $_SESSION['user']['id'];
define (SUID , $_SESSION['user']['id']);//Session User ID

function needAuth(){
    if(!isset($_SESSION['user'])){
        header('HTTP/1.1 403 Need authentification');
        echo'<meta http-equiv="refresh" content="0; url=login.php">';
        die;
    }
}

function create($pdo,$task){
    global $useDB,$taskTable,$SUID;
     
    try {
       $pdo->query("INSERT INTO $useDB.$taskTable( user_id,assigned_user_id,description, is_done, date_added)"
               . " VALUES ('$SUID',NULL,'$task','0',NOW())");
    }  
    catch (PDOException $e) {  
        echo $e->getMessage();
    }
    echo '<span class="success alert">Задача успешно добавлена</span>';
    reload();

}
function delete($pdo,$id){
    global $useDB,$taskTable;
    $pdo->query("DELETE FROM $useDB.$taskTable WHERE id=$id");
    reload();
}


function duplicate(){
    $background = 'danger';
    echo '<span class="danger alert">Такая задача уже есть</span>';
    reload();
}

function doIt($pdo,$id){
    global $useDB,$taskTable;
    $pdo->query("UPDATE $useDB.$taskTable SET `is_done`= 1 WHERE id=$id");
    reload();
}

function read($pdo,$id){
    global $useDB,$taskTable;
    $result=$pdo->query("SELECT description FROM $useDB.$taskTable WHERE id=$id");
    $assocArray = $result->fetch();
    return $assocArray['description'];
}

function changeTask($pdo,$id,$newValue){
    global $useDB,$taskTable;
    $newVal = $pdo->prepare("UPDATE $useDB.$taskTable SET description = :NW WHERE id=$id");
    $newVal->execute(array(':NW'=>$newValue));
    reload();
}

function doSignUp($pdo,$login,$pass){
    global $useDB,$userTable;
    $checkUser = $pdo->query("SELECT login FROM $useDB.$userTable");
    while($row = $checkUser->fetch(PDO::FETCH_ASSOC)){
        if($login == $row['login']){
            $ex = 1;
        }
    }
    if($ex = 1){
        echo 'Такой пользователь уже зарегестрирован!';
    }else{
    $newUser = $pdo->prepare("INSERT INTO $useDB.$userTable(login,password) VALUES(:login,:pass)");
    $newUser->execute(array(':login'=>trim($login),':pass'=>trim(password_hash($pass,PASSWORD_DEFAULT))));
    echo'Пользователь "'.$_POST['login'].'" успешно добавлен!';
    }
}

function checkUser($pdo,$login,$pass){
    global $useDB,$userTable;
    $res = $pdo->query("SELECT id_user,login,password FROM $useDB.$userTable");
    while ($row = $res->fetch(PDO::FETCH_ASSOC)){//$row - ассоциативный массив значений, ключи - названия столбцов
    if(($login===$row['login'])&&(password_verify($pass, $row['password']))){
        return array('login'=>$login,'id'=>$row['id_user']);
    }
}
 echo 'нет такого логина или пароля';
    
}

function transmit($pdo,$idUser,$idTask){
    global $useDB,$taskTable;
    $pdo->query("UPDATE $taskTable SET assigned_user_id =$idUser WHERE id = $idTask");
    reload();
}

function reload(){
     echo'<meta http-equiv="refresh" content="2;URL=/">';
}